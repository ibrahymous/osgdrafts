#include <osg/Program>
#include <osg/MatrixTransform>
#include <osg/Texture2D>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osg/BlendFunc>
#include <osg/io_utils>
#include <osg/Depth>

#include "./mcslices_i.c"


// --- 8< --------------------------------------------------------------------


namespace osgFire {


class SlicedCube : public osg::Geometry {
 public:
  SlicedCube(int slicesCount, int mode = GL_LINE_LOOP);

 private:
  void update(const osg::Vec3 &eye);

 private:
  osg::Vec3 _pos;

friend struct SlicedCubeUpdateCallback;
friend struct SlicedCubeCullCallback;
};


struct SlicedCubeUpdateCallback: osg::Drawable::UpdateCallback {
  void update (osg::NodeVisitor *nv, osg::Drawable *drawable) {
    static_cast<SlicedCube *>(drawable)->update(
        static_cast<SlicedCube *>(drawable)->_pos);
  }
};


struct SlicedCubeCullCallback: osg::Drawable::CullCallback {
  bool cull(osg::NodeVisitor *nv, osg::Drawable *drawable,
      osg::RenderInfo *renderInfo) const {
    static_cast<SlicedCube *>(drawable)->_pos = nv->getViewPoint();
    OSG_WARN << "VIEW: " << nv->getViewPoint() << std::endl;
    return false;
  }
};


SlicedCube::SlicedCube(int slicesCount, int mode) {
  const int maxPointsPerSlice = 6;
  const int maxPoints = slicesCount*maxPointsPerSlice;

  setUseDisplayList(false);
  setUseVertexBufferObjects(true);
  setVertexArray(new osg::Vec3Array(maxPoints));
  setTexCoordArray(0, new osg::Vec3Array(maxPoints));

  for (int i = 0; i < slicesCount; ++i) {
    addPrimitiveSet(new osg::DrawArrays(mode));
  }

  /// \todo Use SINGLETON!
  setUpdateCallback(new SlicedCubeUpdateCallback());
  setCullCallback(new SlicedCubeCullCallback());

  _pos.set(0.0, 0.0, 0.0);
  update(_pos);
}


void SlicedCube::update(const osg::Vec3 &eye) {
  const osg::Vec3 VERTICES[8] = {  /// \todo Make ONCE!
    osg::Vec3( 0,  0,  0),   // 0
    osg::Vec3( 1,  0,  0),   // 1
    osg::Vec3( 0,  1,  0),   // 2
    osg::Vec3( 1,  1,  0),   // 3
    osg::Vec3( 0,  0,  1),   // 4
    osg::Vec3( 1,  0,  1),   // 5
    osg::Vec3( 0,  1,  1),   // 6
    osg::Vec3( 1,  1,  1),   // 7
  };

  const int EDGES[12][2] = {
    {0, 1},   // 0
    {4, 5},   // 1
    {2, 3},   // 2
    {6, 7},   // 3
    {0, 2},   // 4
    {1, 3},   // 5
    {4, 6},   // 6
    {5, 7},   // 7
    {0, 4},   // 8
    {2, 6},   // 9
    {1, 5},   // a
    {3, 7},   // b
  };

  float Z[8];

  float zMin = FLT_MAX;
  float zMax = FLT_MIN;
  for (int i = 0; i < 8; ++i) {
    float z = (VERTICES[i] - eye).length();
    if (z < zMin) zMin = z;
    if (z > zMax) zMax = z;
    Z[i] = z;
  }

  osg::Vec3Array *XYZ = static_cast<osg::Vec3Array *>(getVertexArray());
  osg::Vec3Array *STR = static_cast<osg::Vec3Array *>(getTexCoordArray(0));

  //
  // ..!....!....!....!..
  //
  const int slicesCount = getPrimitiveSetList().size();
  const float dz = (zMax - zMin)/slicesCount;

  osg::Vec3 vec;
  float z = zMin + 0.5*dz;
  int vertexNumber = 0;
  for (int sliceNo = 0; sliceNo < slicesCount; ++sliceNo) {
    int key = 0
      | ((Z[0] > z)?   1 : 0)
      | ((Z[1] > z)?   2 : 0)
      | ((Z[2] > z)?   4 : 0)
      | ((Z[3] > z)?   8 : 0)
      | ((Z[4] > z)?  16 : 0)
      | ((Z[5] > z)?  32 : 0)
      | ((Z[6] > z)?  64 : 0)
      | ((Z[7] > z)? 128 : 0)
      ;

    OSG_WARN
      << "z = " << z
      << ", "
      << "min = " << zMin
      << ", "
      << "max = " << zMax
      << ", "
      << "key = " << key
      << std::endl
      ;

    int firstVertex = vertexNumber;
    for (int i = 0; MCTABLE[key][i] > -1; ++i) {
      int e = MCTABLE[key][i];
      float z0 = Z[EDGES[e][0]];
      float z1 = Z[EDGES[e][1]];
      float r = (z - z0) / (z1 - z0);
      switch (e) {
        case 0:
        case 1:
        case 2:
        case 3:
          vec.set(r, VERTICES[EDGES[e][0]][1], VERTICES[EDGES[e][0]][2]);
          break;

        case 4:
        case 5:
        case 6:
        case 7:
          vec.set(VERTICES[EDGES[e][0]][0], r, VERTICES[EDGES[e][0]][2]);
          break;

        default:  // 8, 9, 10, 11
          vec.set(VERTICES[EDGES[e][0]][0], VERTICES[EDGES[e][0]][1], r);
      }

      OSG_WARN << " - " << vec << std::endl;
      (*XYZ)[vertexNumber].set(vec.x()*4, vec.y()*30.0, vec.z()*4);
      (*STR)[vertexNumber].set(vec);

      ++vertexNumber;
    }

    osg::DrawArrays *ps =
      static_cast<osg::DrawArrays *>(getPrimitiveSet(sliceNo));
    ps->setFirst(firstVertex);
    ps->setCount(vertexNumber - firstVertex);

    z += dz;
  }

  XYZ->dirty();
  STR->dirty();
}


}  // namespace osgFire


// -------------------------------------------------------------------- >8 ---


int main(int argc, char **argv) {
  osg::Geode *geode = new osg::Geode();

#if 1
  osg::Drawable *d1 = new osgFire::SlicedCube(10, GL_TRIANGLE_FAN);
  geode->addDrawable(d1);

  {
    osg::Texture2D *texture = new osg::Texture2D();
    texture->setImage(osgDB::readImageFile("textures/nzw.png"));
    texture->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
    texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
    texture->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
    texture->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);
    texture->setWrap(osg::Texture::WRAP_R, osg::Texture::REPEAT);
    d1->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture);
    d1->getOrCreateStateSet()->addUniform(new osg::Uniform("nzw", 0));
  }

  {
    osg::Texture2D *texture = new osg::Texture2D();
    texture->setImage(osgDB::readImageFile("textures/firetex.png"));
    texture->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
    texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
    texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
    texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
    d1->getOrCreateStateSet()->setTextureAttributeAndModes(1, texture);
    d1->getOrCreateStateSet()->addUniform(new osg::Uniform("fireProfile", 1));
  }

  osg::Program *program = new osg::Program();
  program->addShader(osgDB::readShaderFile("shaders/fire.vert"));
  program->addShader(osgDB::readShaderFile("shaders/fire.frag"));
  d1->getOrCreateStateSet()->setAttributeAndModes(program);
  d1->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
  //d1->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF);

  // Conversely, disable writing to depth buffer so that
  // a transparent polygon will allow polygons behind it to shine thru.
  // OSG renders transparent polygons after opaque ones.
  osg::Depth *depth = new osg::Depth();
  depth->setWriteMask(false);
  d1->getOrCreateStateSet()->setAttributeAndModes(depth, osg::StateAttribute::ON);

  d1->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
  d1->getOrCreateStateSet()->setAttributeAndModes(new osg::BlendFunc(
        osg::BlendFunc::SRC_ALPHA,
        osg::BlendFunc::ONE));
        //osg::BlendFunc::ONE_MINUS_SRC_ALPHA));
#endif

  //osg::Drawable *d2 = new osgFire::SlicedCube(4);
  //geode->addDrawable(d2);

  osg::Group *root = new osg::Group();
  root->addChild(geode);
  root->addChild(osgDB::readNodeFile(argv[1]));



  osg::Geode *geode3 = new osg::Geode();
  osg::Drawable *d3 = new osgFire::SlicedCube(10, GL_TRIANGLE_FAN);
  d3->setStateSet(d1->getOrCreateStateSet());
  geode3->addDrawable(d3);
  osg::MatrixTransform *mt = new osg::MatrixTransform(osg::Matrix::translate(2, 0, 2));
  mt->addChild(geode3);
  root->addChild(mt);


  osgViewer::Viewer viewer;
  viewer.setSceneData(root);
  viewer.addEventHandler(new osgViewer::StatsHandler());
  viewer.getCamera()->setClearColor(osg::Vec4(0.0f,0.0f,0.0f,0.0f));

  return viewer.run();
}

