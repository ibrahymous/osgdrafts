import numpy as np
import math
import sys

VERTICES = (
  (-1, -1, -1),   # 0
  ( 1, -1, -1),   # 1
  (-1,  1, -1),   # 2
  ( 1,  1, -1),   # 3
  (-1, -1,  1),   # 4
  ( 1, -1,  1),   # 5
  (-1,  1,  1),   # 6
  ( 1,  1,  1),   # 7
)


EDGES = (
  (0, 1),   # 0
  (4, 5),   # 1
  (2, 3),   # 2
  (6, 7),   # 3

  (0, 2),   # 4
  (1, 3),   # 5
  (4, 6),   # 6
  (5, 7),   # 7

  (0, 4),   # 8
  (2, 6),   # 9
  (1, 5),   # a
  (3, 7),   # b
)



def edgeVector(iEdge):
    v0 = np.array(VERTICES[EDGES[iEdge][0]])
    v1 = np.array(VERTICES[EDGES[iEdge][1]])
    k = v1 - v0     # normalized by design
    return v0, k


def edgePoint(iEdge, t):
    v0, k = edgeVector(iEdge)
    return v0 + k*t


# ----------------------------------------------------------------------------


def jarvismarch(A, rotate):
  n = len(A)
  P = [i for i in range(n)]
  # start point
  for i in range(1,n):
    if A[P[i]][0]<A[P[0]][0]:
      P[i], P[0] = P[0], P[i]
  H = [P[0]]
  del P[0]
  P.append(H[0])
  while True:
    right = 0
    for i in range(1,len(P)):
      if rotate(A[H[-1]], A[P[right]], A[P[i]]) < 0:
        right = i
    if P[right] == H[0]:
      break
    else:
      H.append(P[right])
      del P[right]
  return H


# ----------------------------------------------------------------------------


def orient(A, K):
    O = A[0]
    a = [i for i in map(lambda x: x - O, A)]

    e1 = a[1]
    e2 = a[2]

    i0 = 0
    i1 = 1
    M = np.array([[e1[i0], e1[i1]], [e2[i0], e2[i1]]])
    if abs(np.linalg.det(M)) < 1e-5: i0 = 2
    M = np.array([[e1[i0], e1[i1]], [e2[i0], e2[i1]]])
    if abs(np.linalg.det(M)) < 1e-5: i1 = 0
    M = np.array([[e1[i0], e1[i1]], [e2[i0], e2[i1]]])

    b = [] #(0, 0), (1, 0), (0, 1)]
    for i in range(len(a)):
        r = a[i]
        p, q = np.linalg.solve(M, [r[i0], r[i1]])
        b.append((p, q))
    new_order = jarvismarch(b,
        lambda A, B, C: K*((B[0]-A[0])*(C[1]-B[1])-(B[1]-A[1])*(C[0]-B[0])))
    return new_order



def check_edges(edges):
    N = 10
    for i0 in range(1, N):
        t0 = float(i0)/N
        r0 = edgePoint(edges[0], t0)
        for i1 in range(1, N):
            t1 = float(i1)/N
            r1 = edgePoint(edges[1], t1)
            for i2 in range(1, N):
                t2 = float(i2)/N
                r2 = edgePoint(edges[2], t2)

                # These two vectors are in the plane
                v1 = r2 - r0
                v2 = r1 - r0

                # the cross product is a vector normal to the plane
                cp = np.cross(v1, v2)
                a, b, c = cp

                # This evaluates a * x3 + b * y3 + c * z3 which equals d
                d = np.dot(cp, r2)

                #
                ok = True
                vv = [r0, r1, r2]
                for iEdge in range(12):
                    if iEdge in edges[0:3]: continue
                    v0, k = edgeVector(iEdge)
                    div = np.dot(cp, k)
                    if abs(div) > 1e-10:
                        t = - (np.dot(cp, v0) - d) / div
                        if 0.0 <= t and t <= 1.0:
                            if not (iEdge in edges):
                                ok = False
                        else:
                            if iEdge in edges:
                                ok = False
                        if ok and (iEdge in edges):
                            vv.append(v0 + k*t)
                    else:
                        pass
                        #print('### complanar:', i, edges, iEdge)

                if ok: return vv, (a, b, c, -d)
    return [], (0, 0, 0, 0)


def reorder(x, idx):
    result = []
    for i in range(len(idx)): result.append(x[idx[i]])
    return result


def buildTable(lines = range(256)):
    result = []
    def append(n, A = []):
        F = lambda A: A + [-1,] * (12 - len(A))
        result.append('  /* %3d */ { %s },' %
                (n, ', '.join("%2d" % i for i in F(A))))

    for i in lines:
        ii = i
        x = [0, 0, 0, 0, 0, 0, 0, 0]
        for k in range(8):
            x[k] = ii % 2
            ii = ii // 2
        edges = []
        for iEdge, edge in enumerate(EDGES):
            if x[edge[0]] == 0 and x[edge[1]] != 0: edges.append(iEdge)
            if x[edge[0]] != 0 and x[edge[1]] == 0: edges.append(iEdge)

        if len(edges) < 3:
            print(i, "Oops! Not enough edges...")
            append(i)
            continue

        if len(edges) > 6:
            print(i, "Oops! Too many edges...")
            append(i)
            continue

        zzz, (a, b, c, d) = check_edges(edges)
        if zzz:
            v = (0, 0, 0)
            for j in range(8):
                if x[j]:
                    v = VERTICES[j]
                    break
            def D(x, plane):
                a, b, c, d = plane
                return (np.dot((a, b, c), x) + d) / math.sqrt(a*a + b*b + c*c)
            K = D(v, (a, b, c, d))
            iii = orient(zzz, K)
            if len(iii) != len(zzz):
                print(i, "OOPS! CONVEX HULL ERROR for", zzz, "plane is", a, b, c, d)
            print(i, len(edges), edges, reorder(edges, iii)) #, reorder(zzz, iii))
            append(i, reorder(edges, iii))
        else:
            print(i, "Oops! Check error for", edges, 'len =', len(edges))
            append(i)

    return result

out = open("mcslices_i.c", "wt")
#out = sys.stdout
out.write("static int MCTABLE[256][12] = {\n")
for i in buildTable():
#for i in buildTable((0, 1, 174, 175, 176, 254, 255)):
#for i in buildTable((23,)):
    out.write(i)
    out.write('\n')
out.write('};\n\n')

