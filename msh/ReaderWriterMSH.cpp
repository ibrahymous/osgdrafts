/*
 * Orbiter Mesh Loader plugin for OpenSceneGraph.
 * Copyright (C) 2015 by Oleg Ibrajev.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Material>
#include <osg/Texture2D>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/ReadFile>
#include <osgDB/ReaderWriter>
#include <osgDB/Registry>
// TODO: Optimize!
//#include <osgUtil/Optimizer>

struct MaterialInfo: public osg::Referenced {
  int _material, _texture;
  MaterialInfo(int material, int texture)
  : _material(material), _texture(texture) {
  }
};


static
osg::Geometry * readGroup(std::istream &stream,
    int nv, int nt, bool no_normal, unsigned flags) {
  std::string line;
  double x, y, z;

  // Arrays
  osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array(nv);
  osg::ref_ptr<osg::Vec3Array> normals = new osg::Vec3Array(nv);
  osg::ref_ptr<osg::Vec2Array> uv = new osg::Vec2Array(nv);
  bool has_normals = !no_normal, has_uv = true;
  for (int i = 0; i < nv; ++i) {
    if (!getline(stream, line)) {
      OSG_FATAL << "unexpected end of file" << std::endl;
      return NULL;
    }

    std::stringstream ss(line);

    ss >> y >> z >> x;
    (*vertices)[i].set(x, -y, z);

    if (!no_normal) {
      ss >> y >> z >> x;
      if (!ss) has_normals = false;
      else (*normals)[i].set(x, -y, z);
    }

    ss >> x >> y;
    if (!ss) has_uv = false;
    else (*uv)[i].set(x, y);
  }

  // Triangles
  osg::ref_ptr<osg::DrawElementsUInt> triangles =
    new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 3*nt);
  for (int i = 0; i < nt; ++i) {
    if (!getline(stream, line)) {
      OSG_FATAL << "unexpected end of file" << std::endl;
      return NULL;
    }
    std::stringstream(line)
      >> (*triangles)[3*i+2]
      >> (*triangles)[3*i+1]
      >> (*triangles)[ 3*i ]
      ;
  }

  osg::Geometry *geometry = new osg::Geometry();
  geometry->setVertexArray(vertices);
  if (has_normals) {
    geometry->setNormalArray(normals, osg::Array::BIND_PER_VERTEX);
  }
  if (has_uv) {
    geometry->setTexCoordArray(0, uv);
  }
  geometry->addPrimitiveSet(triangles);
  return geometry;
}


static
osg::Geode * readGroups(std::istream &stream, int count) {
  std::string line, token, label;
  osg::Geode *geode = new osg::Geode();
  int material = 0, texture = 0, flag = 0;
  bool no_normal = false;
  unsigned flags = 0;
  while (std::getline(stream, line)) {
    std::stringstream ss(line);
    ss >> token;
    if (token.compare("MATERIAL") == 0) {
      ss >> material;
    } else if (token.compare("TEXTURE") == 0) {
      ss >> texture;
    } else if (token.compare("LABEL") == 0) {
      ss >> label;
    } else if (token.compare("TEXWRAP") == 0) {
      OSG_WARN << "TEXWRAP option is not supported now" << std::endl;
    } else if (token.compare("NONORMAL") == 0) {
      OSG_WARN << "NONORMAL option is not supported properly" << std::endl;
      no_normal = true;
    } else if (token.compare("FLAG") == 0) {
      ss >> flags;
      OSG_WARN << "FLAG = " << flags << ": option is not supported now" << std::endl;
    } else if (token.compare("GEOM") == 0) {
      int nv, nt;
      ss >> nv >> nt;
      osg::Geometry *geometry = readGroup(stream, nv, nt, no_normal, flags);
      if (geometry) {
        if (!label.empty()) geometry->setName(label);
        geometry->setUserData(new MaterialInfo(material, texture));
        if (0x02 != (flags & 0x02)) geode->addDrawable(geometry);
      }

      label.clear();
      no_normal = false;
      flags = 0;

      --count;
      if (count == 0) break;
    } else {
      OSG_FATAL
        << "Unknown token in readGroups(): "
        << token
        << std::endl;
    }
  }
  return geode;
}


static
bool readColor(std::istream &stream,
    osg::Vec4 &color, double *shininess = NULL) {
  std::string line;
  if (getline(stream, line)) {
    double foo = 0.0; if (!shininess) shininess = &foo;
    std::stringstream ss(line);
    ss >> color[0] >> color[1] >> color[2] >> color[3] >> *shininess;
    if (0 == *shininess) *shininess = 10;
    *shininess /= 2;
    return true;
  } else {
    return false;
  }
}


static
void readMaterials(std::istream &stream, int count, osg::Geode *geode) {
  std::string line;

  // Material names
  std::vector<std::string> names; names.reserve(count);
  for (int i = 0; i < count; ++i) {
    if (!getline(stream, line)) {
      OSG_FATAL << "unexpected end of file" << std::endl;
      return;
    }
    names.push_back(line);
  }

  // Material props
  osg::Vec4 diffuse, ambient, specular, emission;
  double shininess;
  for (int i = 0; i < count; ++i) {
    /// \todo error processing?..
    getline(stream, line);
    readColor(stream, ambient);
    readColor(stream, diffuse);
    readColor(stream, specular, &shininess);
    readColor(stream, emission);

    osg::ref_ptr<osg::Material> material = new osg::Material();
    material->setColorMode(osg::Material::DIFFUSE);
    material->setDiffuse(osg::Material::FRONT, diffuse);
    material->setAmbient(osg::Material::FRONT, ambient);
    material->setSpecular(osg::Material::FRONT, specular);
    material->setEmission(osg::Material::FRONT, emission);
    material->setShininess(osg::Material::FRONT, shininess);

    for (size_t j = 0; j < geode->getNumDrawables(); ++j) {
      osg::Drawable *drawable = geode->getDrawable(j);
      MaterialInfo *mi = static_cast<MaterialInfo *>(drawable->getUserData());
      if (mi->_material == i + 1) {
        osg::StateSet *st = drawable->getOrCreateStateSet();
        st->setAttributeAndModes(material, osg::StateAttribute::ON);
      }
    }
  }
}


static
void readTextures(std::istream &stream, int count, osg::Geode *geode) {
  std::string line;
  for (int i = 0; i < count; ++i) {
    if (!getline(stream, line)) {
      OSG_WARN
        << "Unexpected end of file or read error"
        << std::endl;
      break;
    }

    std::string filename;
    std::stringstream ss(line);
    ss >> filename;

    std::replace(filename.begin(), filename.end(), '\\', '/');

    osg::Image *image = osgDB::readImageFile(filename);
    if (!image) {
      OSG_WARN
        << "Cannot read image " << filename
        << " for texture #" << i
        << std::endl;
      continue;
    }

    osg::Texture2D *texture = new osg::Texture2D();
    texture->setImage(image);
    texture->setWrap(osg::Texture::WRAP_S, osg::Texture::REPEAT);
    texture->setWrap(osg::Texture::WRAP_T, osg::Texture::REPEAT);

    for (size_t j = 0; j < geode->getNumDrawables(); ++j) {
      osg::Drawable *drawable = geode->getDrawable(j);
      MaterialInfo *mi = static_cast<MaterialInfo *>(drawable->getUserData());
      if (mi->_texture == i + 1) {
        osg::StateSet *st = drawable->getOrCreateStateSet();
        st->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
      }
    }
  }
}


static
osg::Node * readMesh(std::istream &stream) {
  std::string line, token;
  int count;
  osg::Geode *node = NULL;
  while (std::getline(stream, line)) {
    std::stringstream ss(line);
    ss >> token;
    if (token.compare("MSHX1") == 0) {
    } else if (token.compare("GROUPS") == 0) {
      ss >> count;
      if (count) node = readGroups(stream, count);
    } else if (token.compare("MATERIALS") == 0) {
      ss >> count;
      if (count) readMaterials(stream, count, node);
    } else if (token.compare("TEXTURES") == 0) {
      ss >> count;
      if (count) readTextures(stream, count, node);
    } else {
      OSG_FATAL
        << "Unknown token in readMesh(): "
        << token
        << std::endl;
    }
  }

  return node;
}




class ReaderWriterMSH1: public osgDB::ReaderWriter {
 public:
  ReaderWriterMSH1() {
    supportsExtension("msh", "Orbiter Mesh format");
  }

  const char *className() const {
    return "Orbiter Mesh Reader";
  }

  Features supportedFeatures() const {
    return FEATURE_READ_NODE;
  }

  ReadResult readNode(const std::string &file, const Options *options) const {
    std::string ext = osgDB::getFileExtension(file);
    if (!acceptsExtension(ext)) return ReadResult::FILE_NOT_HANDLED;

    std::string fileName = osgDB::findDataFile(file, options);
    if (fileName.empty()) return ReadResult::FILE_NOT_FOUND;

    osgDB::ifstream stream;
    stream.open(fileName.c_str(), std::ios::in);
    if (!stream.is_open()) return ReadResult::FILE_NOT_FOUND;

    osg::ref_ptr<Options> local_opt;
    if (options) {
      local_opt =
        static_cast<Options *>(options->clone(osg::CopyOp::DEEP_COPY_ALL));
    } else {
      local_opt = new Options();
      local_opt->getDatabasePathList().push_back(osgDB::getFilePath(fileName));
	}

    ReadResult result = readNode(stream, local_opt.get());
    if (result.validNode()) result.getNode()->setName(fileName);
    return result;
  }

  ReadResult readNode(std::istream& fin, const Options *options) const {
    return readMesh(fin);
  }

  WriteResult writeNode(const osg::Node &node,
      const std::string &fileName, const Options *options) const {
    return WriteResult::FILE_NOT_HANDLED;
  }

  WriteResult writeNode(const osg::Node &node,
      std::ostream & fout, const Options* opts) const {
    return WriteResult::FILE_NOT_HANDLED;
  }
};


REGISTER_OSGPLUGIN(msh, ReaderWriterMSH1)

